<!--Used For displaying Products in OWL carousel-->
      
              <div id="raffle" class="owl-carousel owl-theme">
                <?php
                // echo $this->getLayout()->createBlock('cms/block')->setBlockId('raffle-carousel')->toHtml()
                    //Fetch attribute set id by attribute set name
                    $attrSetName = 'Raffle';
                    $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                        ->load($attrSetName, 'attribute_set_name')
                        ->getAttributeSetId();
                     
                    //Load product model collecttion filtered by attribute set id
                    $products = Mage::getModel('catalog/product')
                        ->getCollection()
                        ->addAttributeToSelect('*')
                        ->addFieldToFilter('attribute_set_id', $attributeSetId);
                     
                    //process your product collection as per your bussiness logic
                    $productsName = array();
                    foreach($products as $p){ 
                      if ($p->isSaleable()){
                              
                      ?>
                        <div class="item">
                          <a href="<?php echo '/' . $p->getUrlPath(); ?>" alt="<?php echo $p->getData('name'); ?>">
                              <img src="<?php echo Mage::helper('catalog/image')->init($p, 'image')->resize(250, 150);?>">
                              <span><?php echo $p->getData('name'); ?></span>
                          </a>
                        </div>
                     

                      <?php
                      }; 
                      } 
                      ?>
              
              </div>